package com.aios.demo.controllers;

import java.net.URI;
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.aios.demo.commons.VarSystems;
import com.aios.demo.entities.Command;
import com.aios.demo.entities.Recipient;
import com.aios.demo.exceptions.CommandNotFoundException;
import com.aios.demo.exceptions.RecipientNotFoundException;
import com.aios.demo.exceptions.ResourceNotFoundException;
import com.aios.demo.repositories.CommandRepository;
import com.aios.demo.repositories.RecipientRepository;

@RestController
@RequestMapping("/api")
public class CommandController {

	@Autowired 
	private CommandRepository commandRepository;
	
	@Autowired
	private RecipientRepository recipientRepository;
	
	
	@GetMapping("/commands")
	public List<Command> getAllRecipients() {
		
		return commandRepository.findAll();
	}
	
	@GetMapping("/commands/recipients/{recipientId}")
	public List<Command> getAllCommandsOfRecipient(@PathVariable long recipientId) {
		
		
		
		//Optional<Recipient> recipientOptional = recipientRepository.findById(recipientId);
		
		//if(!recipientOptional.isPresent())
		//{
		//	throw new ResourceNotFoundException(String.format("Recipient is not found with id  :  '%s'", recipientId));
			//response.put("ERROR", String.format("Recipient is not found with id : %s", recipientId)); 
			//return ResponseEntity.badRequest().body(response);
		//}
		
		
		return commandRepository.findAll()
				.stream()
				.filter(x -> x.getRecipient().getId().compareTo(recipientId) == 0)
				.collect(Collectors.toList());
	}
	
	@PutMapping("/commands/{commandId}")
	public  ResponseEntity<Object> updateRecipient(@Valid @RequestBody Command command,  @PathVariable long commandId) throws CommandNotFoundException {
		
		
		Map<String,String> response = new HashMap<String, String>();
		
		Optional<Recipient> commandOptional = recipientRepository.findById(commandId);
		
		if(!commandOptional.isPresent())
		{
			throw new CommandNotFoundException(commandId);
			//response.put("ERROR", String.format("Recipient is not found with id : %s", recipientId)); 
			//return ResponseEntity.badRequest().body(response);
		}
		
		command.setId(commandId);
		command.setPrice(command.getQuantity() * VarSystems.price);
		commandRepository.save(command);
		
		response.put("OK", "SUCCESS UPDATING DATA"); 
		
		return ResponseEntity.ok().body(response);
	}
	
	@DeleteMapping("/commands/{id}")
	public Map<String, Boolean> deleteRecipient(@PathVariable long  id) throws ResourceNotFoundException {
		
		
		
		Command command = commandRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(String.format("Command is not found with id  :  '%s'", id)));
		
		commandRepository.delete(command);
	    Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		
		return response;
	}
	
	
	@PostMapping("commands")
	public ResponseEntity<Object> createRecipient(@Valid @RequestBody Command command) {
		
		if(command.getRecipient().getId() != null)
		{
			Optional<Recipient> recipientOptional = recipientRepository.findById(command.getRecipient().getId());
		
			if(recipientOptional.isPresent())
			{
				command.setRecipient(recipientOptional.get());
			}
		}
		
		command.setPrice(command.getQuantity() * VarSystems.price);
		//command.setDeliveryDate(new Date(System.currentTimeMillis()));
		
		Command savedCommand = commandRepository.save(command);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedCommand.getId()).toUri();

		return ResponseEntity.created(location).build();
		
	}
	
}
