package com.aios.demo.controllers;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.aios.demo.entities.Command;
import com.aios.demo.entities.Recipient;
import com.aios.demo.exceptions.RecipientNotFoundException;
import com.aios.demo.exceptions.ResourceNotFoundException;
import com.aios.demo.repositories.CommandRepository;
import com.aios.demo.repositories.RecipientRepository;

@RestController
@RequestMapping("/api")
public class RecipientController {

	@Autowired
	private RecipientRepository recipientRepository;
	
	
	@GetMapping("/recipients")
	public List<Recipient> getAllRecipients() {
		
		return recipientRepository.findAll();
	}
	
	
	@PutMapping("/recipients/{recipientId}")
	public  ResponseEntity<Object> updateRecipient(@Valid @RequestBody Recipient recipient,  @PathVariable long recipientId) throws ResourceNotFoundException {
		
		
		Map<String,String> response = new HashMap<String, String>();
		
		Optional<Recipient> recipientOptional = recipientRepository.findById(recipientId);
		
		if(!recipientOptional.isPresent())
		{
			throw new ResourceNotFoundException(String.format("Recipient is not found with id  :  '%s'", recipient));
			//response.put("ERROR", String.format("Recipient is not found with id : %s", recipientId)); 
			//return ResponseEntity.badRequest().body(response);
		}
		
		recipient.setId(recipientId);
		recipientRepository.save(recipient);
		
		response.put("OK", "SUCCESS UPDATING DATA"); 
		
		return ResponseEntity.ok().body(response);
	}
	
	@DeleteMapping("/recipients/{id}")
	public Map<String, Boolean>  deleteRecipient(@PathVariable long  id) throws ResourceNotFoundException {
		
		//recipientRepository.deleteById(id);
		
		Recipient recipient = recipientRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(String.format("Command is not found with id  :  '%s'", id)));
		
		recipientRepository.delete(recipient);
	    Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		
		return response;
	}
	
	@PostMapping("recipients")
	public ResponseEntity<Recipient> createRecipient(@Valid @RequestBody Recipient recipient) {
		
		if(!recipientRepository.exists(Example.of(recipient)))
		{
			Recipient savedStudent = recipientRepository.save(recipient);

			URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedStudent.getId()).toUri();
		
			return ResponseEntity.created(location).build();
		}
		
		
		return new ResponseEntity<Recipient>(recipient, HttpStatus.FORBIDDEN);
	}	
	
}
