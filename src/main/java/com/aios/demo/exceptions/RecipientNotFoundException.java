package com.aios.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class RecipientNotFoundException extends Exception {

	private long recipientID;
	
	public RecipientNotFoundException(long recipientID) {
		super(String.format("Recipient isnot found with id  :  '%s'", recipientID));
	}
	
	
}
