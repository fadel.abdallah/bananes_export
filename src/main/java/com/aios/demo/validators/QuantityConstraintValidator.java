package com.aios.demo.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class QuantityConstraintValidator implements ConstraintValidator<QuantityConstraint,Integer> {

	@Override
	public boolean isValid(Integer value, ConstraintValidatorContext context) {
		// TODO Auto-generated method stub
		return value % 25 == 0;
	}

	
	
}
