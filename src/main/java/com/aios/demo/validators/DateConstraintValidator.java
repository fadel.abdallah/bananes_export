package com.aios.demo.validators;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DateConstraintValidator implements ConstraintValidator<DateConstraint, Date> {

	@Override
	public boolean isValid(Date value, ConstraintValidatorContext context) {
		// TODO Auto-generated method stub
		Date date = new Date();

		LocalDate localDate1 = Instant.ofEpochMilli(date.getTime())
		      .atZone(ZoneId.systemDefault())
		      .toLocalDate();
		
		LocalDate localDate2 = Instant.ofEpochMilli(value.getTime())
			      .atZone(ZoneId.systemDefault())
			      .toLocalDate();
		 
		return date.compareTo(value) < 0 && Period.between(localDate1, localDate2).getDays() > 7;
	}

	
}
