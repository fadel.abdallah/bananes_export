package com.aios.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter{

	// Create two users for demo
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// TODO Auto-generated method stub
		
		auth.inMemoryAuthentication()
		.withUser("user").password("{noop}password").roles("USER")
		.and()
		.withUser("admin").password("{noop}password").roles("ADMIN");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// TODO Auto-generated method stub
		
		
		// HTTP basic authentification
		
		http.httpBasic()
			.and()
			.authorizeRequests()
			.antMatchers(HttpMethod.GET, "/api/recipients/**").hasRole("USER")
			.antMatchers(HttpMethod.GET, "/api/commands/**").hasRole("USER")
			.antMatchers(HttpMethod.GET, "/api/commands/recipients/**").hasRole("USER")
			
			
			.antMatchers(HttpMethod.POST, "/api/recipients/").hasRole("ADMIN")
			.antMatchers(HttpMethod.POST, "/api/commands/").hasRole("ADMIN")
			
			
			.antMatchers(HttpMethod.PUT, "/api/recipients/**").hasRole("ADMIN")
			.antMatchers(HttpMethod.PUT, "/api/commands/**").hasRole("ADMIN")
			
			.antMatchers(HttpMethod.DELETE, "/api/recipients/**").hasRole("ADMIN")
			.antMatchers(HttpMethod.DELETE, "/api/commands/**").hasRole("ADMIN")
			
			.and().csrf().disable().formLogin().disable();
		
		
	
	}
	
	
	
	
}
