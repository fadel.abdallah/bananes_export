package com.aios.demo.errors;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

public class ErrorDetails {
	
	private int errorCode;
	private String errorString;
	private String errorMessage;
	private List<String> fieldErrorStrings = new ArrayList<>();
	
	
	public ErrorDetails(HttpStatus httpStatus, String errorMessage, List<String> fieldErrorStrings) {
		super();
		this.errorCode = httpStatus.value();
		this.errorString =  httpStatus.name();
		this.errorMessage = errorMessage;
		this.fieldErrorStrings = fieldErrorStrings;
	}


	public int getErrorCode() {
		return errorCode;
	}


	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}


	public String getErrorString() {
		return errorString;
	}


	public void setErrorString(String errorString) {
		this.errorString = errorString;
	}


	public String getErrorMessage() {
		return errorMessage;
	}


	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}


	public List<String> getFieldErrorStrings() {
		return fieldErrorStrings;
	}


	public void setFieldErrorStrings(List<String> fieldErrorStrings) {
		this.fieldErrorStrings = fieldErrorStrings;
	}


}
