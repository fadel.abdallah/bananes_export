package com.aios.demo.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "recipient")
public class Recipient  {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="name")
	@NotBlank(message = "Name is mandatory")
	private String name;
	
	@Column(name="address")
	@NotBlank(message = "Address is mandatory")	
	private String address;
	
	@Column(name="postal_code")
	@NotBlank(message = "Postal Code is mandatory")
	private String postalCode;
	
	@Column(name="city")
	@NotBlank(message = "City Code is mandatory")
	private String city;
	
	@Column(name="country")
	@NotBlank(message = "Country Code is mandatory")
	private  String country;
	
	//@OneToMany(
			//fetch = FetchType.LAZY,
	//		cascade = CascadeType.ALL,
	//		mappedBy = "recipient")
	//private Set<Command> commands  = new HashSet<>();

	public Recipient() {
		
	}

	public Recipient(@NotBlank(message = "Name is mandatory") String name,
			@NotBlank(message = "Address is mandatory") String address,
			@NotBlank(message = "Postal Code is mandatory") String postalCode,
			@NotBlank(message = "City Code is mandatory") String city,
			@NotBlank(message = "Country Code is mandatory") String country) {
		
		this.name = name;
		this.address = address;
		this.postalCode = postalCode;
		this.city = city;
		this.country = country;
	}

	public Recipient(Long id, @NotBlank(message = "Name is mandatory") String name,
			@NotBlank(message = "Address is mandatory") String address,
			@NotBlank(message = "Postal Code is mandatory") String postalCode,
			@NotBlank(message = "City Code is mandatory") String city,
			@NotBlank(message = "Country Code is mandatory") String country) {
		
		this.id = id;
		this.name = name;
		this.address = address;
		this.postalCode = postalCode;
		this.city = city;
		this.country = country;
	}
	
	public Recipient(Recipient recipient) {
		this.name = recipient.name;
		this.address = recipient.address;
		this.postalCode = recipient.postalCode;
		this.city = recipient.city;
		this.country = recipient.country;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	
	
	
	
//	public Set<Command> getCommands() {
//		return commands;
//	}
//
//	public void setCommands(Set<Command> commands) {
//		this.commands = commands;
//	}

	@Override
	public String toString() {
		return "Recipient [id=" + id + ", name=" + name + ", address=" + address + ", postalCode=" + postalCode
				+ ", city=" + city + ", country=" + country + "]";
	}
	
	
	
	
	
	
}
