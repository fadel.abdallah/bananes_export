package com.aios.demo.entities;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import com.aios.demo.validators.DateConstraint;
import com.aios.demo.validators.QuantityConstraint;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "command")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Command {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@JsonFormat(pattern = "dd/MM/yyyy")
	@NotNull(message = "The delivery date is required.")
	@DateConstraint(message = "The delivery date must be in one week from now")
	@Column(name = "delivery_date")
	private Date deliveryDate;

	@Range(min = 25, max = 1000, message = "Quantity is mandatory and must be between 0 and 1000")
	@QuantityConstraint(message = "The quantity of bananas must be a multiple of 25")
	@Column(name="quantity")
	private int quantity;
	
	@Column(name="price")
	private double price;
	
	
	
	@OneToOne(cascade= CascadeType.ALL)
    @JoinColumn(name = "recipient_id", nullable = false)
	private Recipient recipient;

	
	
	public Command() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Command(Date deliveryDate, int quantity, double price, Recipient recipient) {
		super();
		this.deliveryDate = deliveryDate;
		this.quantity = quantity;
		this.price = price;
		this.recipient = recipient;
	}



	public Command(Long id, Date deliveryDate, int quantity, double price, Recipient recipient) {
		super();
		this.id = id;
		this.deliveryDate = deliveryDate;
		this.quantity = quantity;
		this.price = price;
		this.recipient = recipient;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Date getDeliveryDate() {
		return deliveryDate;
	}


	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}


	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		this.price = price;
	}

	public Recipient getRecipient() {
		return recipient;
	}


	public void setRecipient(Recipient recipient) {
		this.recipient = recipient;
	}


	@Override
	public String toString() {
		return "Command [id=" + id + ", deliveryDate=" + deliveryDate + ", quantity=" + quantity + ", price=" + price
				+ ", recipient=" + recipient + "]";
	}
	
	
	
}
