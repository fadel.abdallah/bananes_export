package com.aios.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aios.demo.entities.Command;

@Repository
public interface CommandRepository extends JpaRepository<Command, Long>{

}
