package com.aios.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aios.demo.entities.Recipient;

@Repository
public interface RecipientRepository extends JpaRepository<Recipient, Long> {

}
